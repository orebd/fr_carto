from django.contrib import admin

# Register your models here.

from .models import Poi, Favori

admin.site.register(Poi)
admin.site.register(Favori)