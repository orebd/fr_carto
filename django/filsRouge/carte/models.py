from django.db import models
from django.contrib.auth.models import User

from django.conf import settings
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
#from carte.models import Poi


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
        fav = Favori.objects.create(name="Perso", owner=instance)
        fav.users_Allowed.add(instance.id)


# Create your models here.

class Favori(models.Model):
    owner = models.ForeignKey(
        User, related_name='F_ownerID', on_delete=models.CASCADE)
    users_Allowed = models.ManyToManyField(User, related_name='allowUsers')
    name = models.CharField(max_length=200)

    # Surcharge par le nom du Favori et non l'ID
    def __str__(self):
        return self.name


class Poi(models.Model):
    owner = models.ForeignKey(
        User, related_name='P_ownerID', on_delete=models.CASCADE)
    favori = models.ForeignKey(
        Favori, related_name='favoriID', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    lat = models.CharField(max_length=200)
    lon = models.CharField(max_length=200)
    comments = models.CharField(blank=True, null=True, max_length=200)

    # Surcharge par le nom du Poi et non l'ID
    def __str__(self):
        return self.name

    # def save(self, *args, **kwargs):

    #     super(Poi, self).save(*args, **kwargs)

# @receiver(pre_save, sender=Poi)
# def onwerID(sender, instance=None, created=True, **kwargs):
#     if created:
#         #Token.objects.create(user=instance)
#         instance.owner.id = instance.user.id
