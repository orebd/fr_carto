# Generated by Django 3.0.2 on 2020-02-11 16:43

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('carte', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='favori',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='F_ownerID', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='poi',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='P_ownerID', to=settings.AUTH_USER_MODEL),
        ),
    ]
