from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User, Group
from carte.serializers import PoiSerializer, FavoriSerializer, UserSerializer, CreateUserSerializer
from carte.models import Poi, Favori
from rest_framework import viewsets, permissions
from django.shortcuts import render
from rest_framework.permissions import BasePermission, IsAuthenticated, IsAdminUser, SAFE_METHODS
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser

# Create your views here.


class PoiViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]

    queryset = Poi.objects.all().order_by("name")
    serializer_class = PoiSerializer

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        user = self.request.user
        return self.queryset.filter(owner=user).order_by("name")

    # def list(self, request):
    #     queryset = Poi.objects.all().filter(owner=request.user.id)
    #     serializer = PoiSerializer(queryset, many=True)
    #     serializer.data
    #     return Response(serializer.data)

    def create(self, request, pk=None):
        #request.data.owner = request.user.id
        fav = Favori.objects.get(id=request.data['favori'])
        poi = Poi.objects.create(
            owner=request.user,
            comments=request.data['comments'],
            lat=request.data['lat'],
            lon=request.data['lon'],
            name=request.data['name'],
            favori=fav
        )
        queryset = Poi.objects.get(id=poi.id)
        serializer = PoiSerializer(queryset, many=False)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class FavoriViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]

    queryset = Favori.objects.all().order_by("name")
    serializer_class = FavoriSerializer

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        user = self.request.user
        return self.queryset.filter(owner=user).order_by("name")

    def list(self, request, pk=None):
        queryset = Favori.objects.all().filter(
            users_Allowed=request.user.id).order_by("name")
        serializer = FavoriSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, pk=None):
        #request.data.owner = request.user.id
        fav = Favori.objects.create(
            owner=request.user,
            name=request.data['name'],
        )
        fav.users_Allowed.add(request.user)
        queryset = Favori.objects.get(id=fav.id)
        serializer = FavoriSerializer(queryset, many=False)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class UsersViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = User.objects.all().order_by("username")
    serializer_class = UserSerializer

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        user = self.request.user
        return self.queryset.filter(username=user).order_by("username")


# class GroupsViewSet(viewsets.ModelViewSet):
#     permission_classes = [IsAuthenticated]
#     queryset = Group.objects.all().order_by("name")
#     serializer_class = GroupSerializer

# #### ne sert a rien
#     @action(methods=['post'], detail=True)
#     def createuser(self, request, pk=None):
#         print('ok')

class CreateUserViewSet(viewsets.ViewSet):
    """
    Example empty viewset demonstrating the standard
    actions that will be handled by a router class.

    If you're using format suffixes, make sure to also include
    the `format=None` keyword argument for each action.
    """
    # queryset = User.objects.all().order_by("username")
    # serializer_class = UserSerializer

    def create(self, request, pk=None):
        data = JSONParser().parse(request)
        user = User.objects.create_user(
            username=data['username'],
            email=data['email'],
            password=data['password'],
        )
        queryset = User.objects.get(id=user.id)
        serializer = CreateUserSerializer(queryset, many=False)

        return Response(serializer.data, status=status.HTTP_201_CREATED)
