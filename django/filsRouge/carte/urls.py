from django.urls import path, include
from rest_framework import routers
from . import views


router = routers.DefaultRouter()
router.register(r'poi', views.PoiViewSet)
router.register(r'favori', views.FavoriViewSet)
router.register(r'users', views.UsersViewSet)
router.register(r'createuser', views.CreateUserViewSet, 'createuser')
# router.register(r'groups', views.GroupsViewSet)

urlpatterns = [
    path('api/v1/', include(router.urls))
]
from rest_framework.authtoken import views
urlpatterns += [
    path('auth/', views.obtain_auth_token)
]
