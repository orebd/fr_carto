from rest_framework import serializers
from carte.models import Poi, Favori
from django.contrib.auth.models import User, Group


class FavoriSerializer(serializers.ModelSerializer):
    F_ownerID = serializers.PrimaryKeyRelatedField(many=False, read_only=True)
    users_Allowed = serializers.PrimaryKeyRelatedField(
        many=True, queryset=User.objects.all())

    class Meta:
        model = Favori
        #fields = ['url', 'F_ownerID', 'F_usersAllowed', 'name']
        fields = '__all__'


class PoiSerializer(serializers.ModelSerializer):
    P_ownerID = serializers.PrimaryKeyRelatedField(many=False, read_only=True)
    favoriID = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    class Meta:
        model = Poi
        #fields = ["id", "name", "lat", "lon", "comments", "favori"]
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        #fields = ('username', 'email', 'first_name', 'last_name')
        fields = '__all__'


class CreateUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'email')
        #fields = '__all__'


# class GroupSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = Group
#         #fields = ('id', 'name')
#         fields = '__all__'
