from carte.models import Poi, Favori
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

# Anonyme


class UserTestsAnonym(APITestCase):
    def test_Anonym_create_account(self):
        """
        Anonymous can't create a new account object.
        """
        data = {'username': 'DabApps',
                'password': 'ldfjkslmqfhjmqsjmlijfqrsn', 'email': 'fdksjqm'}
        response = self.client.post('/api/v1/createuser/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(response.data['username'], data['username'])
        self.assertEqual(User.objects.get().username, data['username'])


class CreateFavoriTestsAnonym(APITestCase):
    def setUp(self):
        User.objects.create(username="lion")

    def test_Anonym_create_favori(self):
        """
        Anonymous can't create a new Favori object.
        """
        data = {"users_Allowed": [1], "name": "oofksjflsj", "owner": 1}
        response = self.client.post('/api/v1/favori/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # self.assertEqual(Favori.objects.count(), 1)
        # self.assertEqual(Favori.objects.get(
        #   id=response.data['id']).name, data['name'])
        #self.assertEqual(Favori.objects.get(id=response.data['id']).users_Allowed, data['users_Allowed'])
        #self.assertEqual(Favori.objects.get(id=response.data['id']).owner, User.objects.get(id=data['owner']))


class CreatePoiTestsAnonym(APITestCase):
    def setUp(self):
        user = User.objects.create(username="lion")
        fav = Favori.objects.create(name="ok", owner=user)
        fav.users_Allowed.add(user)

    def test_Anonym_create_poi(self):
        """
        Anonymous can't create a new account object.
        """
        data = {"name": "45", "lat": "45", "lon": "45",
                "comments": "45", "owner": 1, "favori": 1}
        response = self.client.post('/api/v1/poi/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        #self.assertEqual(response.data['name'], data['name'])
        # print(response.data)

    def test_Anonym_get_list_POI(self):
        """
        Anonymous can't GET a POI object.
        """
        response = self.client.get('/api/v1/poi/', format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        #self.assertEqual(response.data, [])
        # print(response.data)

# Auth


class CreateFavoriTestsAuth(APITestCase):
    def setUp(self):
        User.objects.create(
            username='lion', email='lennon@thebeatles.com', password='okokokok')

    def test_Auth_LOGGED_create_favori(self):
        """
        Auth User can't create a new Favori object.
        """
        token = Token.objects.get(user__username='lion')
        print(token)
        data = {"name": 'lion'}
        ## wrong format POST need token
        response = self.client.post('/api/v1/favori/', data, format='json')
        print(User.objects.get().password)
        #print('reponse', response.data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        #self.assertEqual(Favori.objects.count(), 0)
        #self.assertEqual(Favori.objects.get(id=response.data['id']).name, data['name'])
        #self.assertEqual(Favori.objects.get(id=response.data['id']).users_Allowed, data['users_Allowed'])
        #self.assertEqual(Favori.objects.get(id=response.data['id']).owner, User.objects.get(id=data['owner']))

    def test_Auth_NOT_LOGGED_create_favori(self):
        """
        Auth User can create a new Favori object.
        """
        data = {"users_Allowed": [1], "name": "oofksjflsj", "owner": 1}
        response = self.client.post('/api/v1/favori/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        #self.assertEqual(Favori.objects.count(), 0)
        #self.assertEqual(Favori.objects.get(id=response.data['id']).name, data['name'])
        #self.assertEqual(Favori.objects.get(id=response.data['id']).users_Allowed, data['users_Allowed'])
        #self.assertEqual(Favori.objects.get(id=response.data['id']).owner, User.objects.get(id=data['owner']))
