# Projet fil-rouge

Vous travaillerez sur une application de votre invention. De l’idée de départ jusqu’à son développement et sa mise en œuvre, votre projet Fil-Rouge constituera un lien entre tous les enseignements, et la démonstration de votre SAVOIR-FAIRE.
Vous serez 4 pour developper votre application pour une durée d'un jours par semaine sur 2 mois.


Les [scénarios](https://gitlab.com/orebd/fr_carto/-/wikis/home) sont disponnibles dans le Wiki

[Démo Front](http://n7_carto.surge.sh)

[Démo Front + Back](http://carto.demessi.fr)

## Django

### Installer Django

ne pas oublier d'utiliser un environnement virtuel avec :
```source /chemin du repertoire/venv/bin/activate```

``` pip3 install -r requirements.txt ```

### Utilisation

Lancer le serveur
``` python manage.py runserver ```

L'interface admin est activé par defaut http://127.0.0.1:8000/admin
Log: admin
Pass: admin

l'API est dispo pour les requetes JSON http://127.0.0.1:8000/api/v1/poi/

#### Table Poi

Poi{name, lat, lon, comment}