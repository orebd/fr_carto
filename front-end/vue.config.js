module.exports = {
  // devServer: {
  //   proxy: '${process.env.VUE_APP_ROOT_API}'
  // }
  // pwa: {
  //   name: 'My App',
  //   themeColor: '#4DBA87',
  //   msTileColor: '#000000',
  //   appleMobileWebAppCapable: 'yes',
  //   appleMobileWebAppStatusBarStyle: 'black',
  //   // configure the workbox plugin
  //   workboxPluginMode: 'InjectManifest',
  //   workboxOptions: {
  //     // swSrc is required in InjectManifest mode.
  //     swSrc: 'src/sw.js'
  //     // ...other Workbox options...
  //   }
  // }
  'transpileDependencies': [
    'vuetify'
  ],

  pwa: {
    name: 'Carto'
  }
}

// WARNING

// When devServer.proxy is set to a string, only XHR requests will be proxied. If you want to test an API URL, don't open it in the browser, use an API tool like Postman instead.
