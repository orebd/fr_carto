import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    props: true
    /* meta: {
      needAuthentification: false
    } */
  },
  {
    path: '/login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes
})

/* router.beforeEach((to, from, next) => {
  if (to.meta.needAuthentification && MyStore.isAuthentification) next({ path: '/' })
  else next('/login')
  console.log(MyStore.isAuthentification, to, from)
}) */

export default router
