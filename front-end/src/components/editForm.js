import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    categories: ['perso']
  },
  mutations: {
    SET_CATEGORIES (state, categories) {
      state.categories = categories
    }
  },
  actions: {
    getCategories ({ commit }) {
      fetch('http://127.0.0.1:8000/api/v1/favori', { headers: { Authorization: 'Token: e16c3b7ec017feca4520661619ee2c17cab15f80' } })
        .then(r => r.json().data)
        .then(categories => {
          commit('SET_CATEGORIES', categories)
        })
    }
  },
  getters: {
    categories: state => state.categories
  },
  modules: {
  }
})
