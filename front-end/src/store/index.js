import Vue from 'vue'
import Vuex from 'vuex'
import myStore from '@/store/myStore'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    categories: ['pro']
  },
  mutations: {
    SET_CATEGORIES (state, categories) {
      state.categories = categories
    }
  },
  actions: {
    getCategories ({ commit }) {
      const response = fetch(`${process.env.VUE_APP_ROOT_API}/favori`, {
        method: 'GET',
        headers: {
          'Authorization': `Token ${myStore.token}`,
          'Content-Type': 'application/json',
          'Accept': '*/*'
        }
      })
        .then(response => response.json())
      commit('SET_CATEGORIES', response.data)
    }
  },
  getters: {
    categories: state => state.categories
  },
  modules: {
  }
})
