export default {
  Markers: [],
  MarkerSelected: [],
  MarkerDragued: {},
  Formulaire: {
    marker: {},
    on: false,
    mode: ''
  },
  FetchPoiParam: {
    method: 'GET',
    url: `${process.env.VUE_APP_ROOT_API}/poi/`
  },
  center: { lat: 47.41322, lon: -1.219482 },
  showLoggin: true,
  favori: [],
  category: [],
  theAddress: 'unknown'
}
